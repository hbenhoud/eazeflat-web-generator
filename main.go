package main

import (
	"flag"
	"fmt"
	"html/template"
	"log"
	"os"
)

const (
	webPageTpl = "static/page.html.template"
)

var (
	pageFileFlag string
	urlTourFlag  string
)

type htmlData struct {
	URLTour string
}

func parseFlags() {
	flag.StringVar(&pageFileFlag, "file", "", "set a web page file name (required)")
	flag.StringVar(&urlTourFlag, "tour", "", "set a virtual tour URL (required)")
	flag.Parse()
	if pageFileFlag == "" || urlTourFlag == "" {
		flag.Usage()
		os.Exit(1)
	}
}

func init() {
	parseFlags()
}

func buildWebPage(fileName string, data htmlData) error {
	var (
		tpl        *template.Template
		outputFile *os.File
		err        error
	)
	// parse files
	if tpl, err = template.ParseFiles(webPageTpl); err != nil {
		return err
	}
	// create output file
	if outputFile, err = os.Create(fmt.Sprintf("%s.html", fileName)); err != nil {
		return err
	}
	defer outputFile.Close()
	// apply data to the parsed template
	if err = tpl.Execute(outputFile, data); err != nil {
		return err
	}
	return nil
}

func main() {
	if err := buildWebPage(pageFileFlag, htmlData{
		URLTour: urlTourFlag,
	}); err != nil {
		log.Fatal("failed to build a web page, err: ", err.Error())
	}
}
